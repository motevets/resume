#!/usr/bin/env bash

set -eu pipefail

node yamlToJson.js > resume.json
mkdir -p public
cp index.html photo.jpg resume.yml resume.json public
npx resume export public/resume.pdf --theme jsonresume-theme-macchiato
npx resume export public/resume.html --theme jsonresume-theme-macchiato
rm resume.json
